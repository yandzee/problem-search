#!/usr/bin/env bash

HEROKU_APP_TAG="problem-search"
LOCAL_DOCKER_TAG="problem-search"
SERVER_PORT="9190"

function faded() {
    echo -e "\033[2m$1\033[0m"
}

function red() {
    echo -e "\033[31m$1\033[0m"
}

function bred() {
    echo -e "\033[1;31m$1\033[0m"
}

function green() {
    echo -e "\033[92m$1\033[0m"
}

function bgreen() {
    echo -e "\033[1;92m$1\033[0m"
}

function bold() {
    echo -e "\033[1;39m$1\033[0m"
}

function show_usage() {
    echo "Available commands:"
    echo "    ▪ $(bold run) [docker | local]"
    echo "    ▪ $(bold build) [run | docker]"
    echo "    ▪ $(bold deploy)"
    echo "    ▪ $(bold release)"
}

function unknown_command() {
    echo "Unknown command $1."

    show_usage
}

function deploy() {
    local tag=$HEROKU_APP_TAG

    heroku container:push web -a $tag
    heroku container:release web -a $tag
    heroku releases -a $tag
}

function run_main() {
    local target=${1:-"docker"}
    local port=${2:-"9290"}

    if [ "$target" == "docker" ]; then
        local container_id=$(docker container ls -f "name=$LOCAL_DOCKER_TAG" -q)
        if [[ ! -z "$container_id" ]]; then
            docker stop $container_id
        fi

        docker run --rm -p $port:$SERVER_PORT \
            --name=$LOCAL_DOCKER_TAG \
            --network="host" \
            $LOCAL_DOCKER_TAG
    fi

    if [ "$target" == "local" ]; then
        eval $(egrep -v '^#' dev.env | xargs) npm start
    fi
}

function build() {
    local target=${1:-"docker"}

    if [ "$target" == "docker" ]; then
        docker build --rm -t $LOCAL_DOCKER_TAG -f Dockerfile .

        if [ "$2" == "run" ]; then
            run_main
        fi
    fi

    if [ "$target" == "local" ]; then
        if [ "$2" == "run" ]; then
            run_main local
        fi
    fi
}

function release() {
    local tag=$HEROKU_APP_TAG
    local deploy_env=${1:-"staging"}

    if [ "$deploy_env" == "production" ] || [ "$deploy_env" == "prod" ]; then
        tag=$FRONTEND_PROD_APP
        deploy_env="production"
    fi

    if [ "$deploy_env" == "staging" ]; then
        tag=$HEROKU_APP_TAG
        deploy_env="staging"
    fi

    heroku container:push web -a $tag --recursive
    heroku container:release web -a $tag
    heroku releases -a $tag
}

function run() {
    local main_cmd=${1:-"status"}
    shift

    echo -n "$(faded "Running command ")"
    echo "$(bold $main_cmd)"
    echo ""

    case $main_cmd in
        deploy)
            deploy $@
            ;;
        build)
            build $@
            ;;
        run)
            run_main $@
            ;;
        release)
            release $@
            ;;
        *)
            unknown_command $main_cmd
            ;;
    esac

}

run $@

const Koa = require('koa');
const staticServe = require('koa-static');

const setupFrontendRoutes = require('./frontend');

const setupRouting = (config, searchServer, frontendServer) => {
  const app = new Koa();

  // Static serving
  console.log(`serving static from: ${config.frontend.staticPath}`);
  app.use(staticServe(config.frontend.staticPath))

  setupFrontendRoutes(app, config, frontendServer);

  return app;
};

module.exports = setupRouting;

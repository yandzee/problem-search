const route = require('koa-route');

const setupFrontendRoutes = (koaApp, config, frontendServer) => {
  const indexPageRoute = route.get('/', indexPageHandler(config, frontendServer));

  koaApp.use(indexPageRoute);
};

const indexPageHandler = (config, frontendServer) => {
  return async (ctx) => {
    ctx.body = await frontendServer.renderIndexPage();
  }
};

module.exports = setupFrontendRoutes;

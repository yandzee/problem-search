const config = require('./config');
const setupRouting = require('./routing');

const SearchServer = require('./search-server');
const Frontend = require('./frontend');

class App {
  constructor() {
    this.config = config;
    this.searchServer = new SearchServer(config);
    this.frontend = new Frontend(config, this.searchServer)

    this.routingApp = setupRouting(config,
      this.searchServer,
      this.frontend,
    );
  }

  run() {
    console.log(`listening at ${this.webPort}`);
    this.routingApp.listen(this.webPort);
  }

  get webPort() {
    return this.config.server.port;
  }
}

module.exports = App;

FROM node:10-alpine

WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . ./

EXPOSE 9190

ENTRYPOINT ["node"]
CMD ["index.js"]

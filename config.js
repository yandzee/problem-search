const path = require('path');

const env = (varName, def) => {
  const envValue = process.env[varName];
  return envValue !== undefined ? envValue : def;
};

const p = (relPath) => {
  return path.join(__dirname, relPath);
};

module.exports = {
  server: {
    port: env('PORT', 9190),
  },
  frontend: {
    staticPath: p('./frontend/static'),
  },
}
